# SimpleEQ

Simple EQ plugin implemented using JUCE framework.

<img src="/Screenshots/Main.png" alt="Screenshot" width="550" height="400">

Plugin was created following <a href="https://www.youtube.com/watch?v=i_Iq4_Kd7Rc" target="_blank">this</a> freeCodeCamps tutorial.

